#!/bin/bash

case $1 in
"dark")
	sed -i -e 's/^import = \["~\/.config\/alacritty\/themes.*$/import = ["~\/.config\/alacritty\/themes\/dark.toml"]/' ~/.config/alacritty/alacritty.toml
	;;
"light")
	sed -i -e 's/^import = \["~\/.config\/alacritty\/themes.*$/import = ["~\/.config\/alacritty\/themes\/light.toml"]/' ~/.config/alacritty/alacritty.toml
	;;
*)
	echo default
	;;
esac
