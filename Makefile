.DEFAULT_GOAL := sync

backup:
	@./dotfiles.sh --backup

restore:
	@./dotfiles.sh --restore

archlinux:
	paru -S --skipreview --assume-installed base-devel cmake unzip unrar p7zip ninja libvips librsvg libappindicator-gtk3 gtk3 appmenu-gtk-module openssl file webkit2gtk curl less grub-btrfs man-db neofetch wl-clipboard fish zsh pacman-contrib zoxide vim linux-headers seahorse timeshift timeshift-autosnap htop acpid jq acpi ripgrep starship tokei fd bat rate-mirrors mutter-dynamic-buffering adw-gtk3-git apple_cursor bob fnm-bin python-pip lua luajit visual-studio-code-bin postman-bin docker docker-buildx docker-compose kitty-git wezterm-git alacritty-git foot github-cli dbeaver shellcheck shfmt stylua mongodb-compass-beta-bin hyperfine tmux-git diff-so-fancy lazygit-git go xf86-video-intel vulkan-intel apple-fonts noto-fonts noto-fonts-cjk noto-fonts-extra ttf-apple-emoji ttf-cascadia-code-nerd tuned cloudflare-warp-bin popcorntime-bin firefox-developer-edition google-chrome ibus-bamboo-git microsoft-edge-dev-bin trashy fragments ufw easyeffects zam-plugins calf lsp-plugins bind

npm:
	npm i -g npm pnpm yarn neovim @nestjs/cli @styled/typescript-styled-plugin typescript-styled-plugin autocannon
	pnpm install-completion zsh
	pnpm install-completion fish

ssh:
	ssh-keygen -t ed25519 -C "thanhvule0310@gmail.com"
	eval "$(ssh-agent -s)"
	ssh-add ~/.ssh/id_ed25519
	wl-copy <~/.ssh/id_ed25519.pub

zsh/omz:
	./helpers/omz.sh

zsh/plugins:
	./helpers/zsh_plugins.sh
